<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSizes extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $table = "product_sizes";

    protected $fillable = [
        'id_product', 'size', 'price'
    ];
}
