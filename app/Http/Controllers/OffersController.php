<?php

namespace App\Http\Controllers;

use App\Http\Requests\OfferStoreRequest;
use App\Offer;
use Illuminate\Http\Request;

class OffersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Index resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $offers = Offer::get();

        return response()->json([
            'data' => $offers
        ]);
    }

    /**
     * Get single resource
     *
     * @param Offer $Offer
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show( Offer $offer ) {

        return response()->json([
            'data' => $offer
        ]);
    }

    /**
     * Update single resource
     *
     * @param OfferStoreRequest $request
     * @param Offer $Offer
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( OfferStoreRequest $request, Offer $offer ) {
        $offer->fill($request->all());
        $offer->save();

        return response()->json([
            'status' => true,
            'data' => $offer
        ]);
    }

    /**
     * Store new resource
     *
     * @param OfferStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store( OfferStoreRequest $request ) {
        $offer = new Offer;
        $offer->fill($request->all());
        $offer->save();

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $offer->id
            ]
        ]);
    }

    /**
     * Destroy single resource
     *
     * @param Offer $Offer
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy( Offer $offer ) {
        $offer->delete();

        return response()->json([
            'status' => true
        ]);
    }

    /**
     * Destroy resources by ids
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroyMass( Request $request ) {
        $request->validate([
            'ids' => 'required|array'
        ]);

        Offer::destroy($request->ids);

        return response()->json([
            'status' => true
        ]);
    }
}
