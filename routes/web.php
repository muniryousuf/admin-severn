<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*
 * Clients management
 * */
Route::prefix('/clients')->group(function () {
    Route::get('', 'ClientsController@index');
    Route::get('{client}', 'ClientsController@show');
    Route::post('store', 'ClientsController@store');
    Route::patch('{client}', 'ClientsController@update');
    Route::post('destroy', 'ClientsController@destroyMass');
    Route::delete('{client}/destroy', 'ClientsController@destroy');
});

Route::prefix('/offers')->group(function () {
    Route::get('', 'OffersController@index');
    Route::get('{offer}', 'OffersController@show');
    Route::post('store', 'OffersController@store');
    Route::patch('{offer}', 'OffersController@update');
    Route::post('destroy', 'OffersController@destroyMass');
    Route::delete('{offer}/destroy', 'OffersController@destroy');
});

Route::prefix('/vouchers')->group(function () {
    Route::get('', 'VouchersController@index');
    Route::get('{voucher}', 'VouchersController@show');
    Route::post('store', 'VouchersController@store');
    Route::patch('{voucher}', 'VouchersController@update');
    Route::post('destroy', 'VouchersController@destroyMass');
    Route::delete('{voucher}/destroy', 'VouchersController@destroy');
});

Route::prefix('/deals')->group(function () {
    Route::get('', 'DealsController@index');
    Route::get('{deal}', 'DealsController@show');
    Route::post('store', 'DealsController@store');
    Route::patch('{deal}', 'DealsController@update');
    Route::post('destroy', 'DealsController@destroyMass');
    Route::get('deals-products', 'DealsController@getDealProducts');
    Route::delete('{deal}/destroy', 'DealsController@destroy');
});

Route::prefix('/categories')->group(function () {
    Route::get('', 'CategoryController@index');
    Route::get('{category}', 'CategoryController@show');
    Route::post('store', 'CategoryController@store');
    Route::patch('{category}', 'CategoryController@update');
    Route::post('destroy', 'CategoryController@destroyMass');
    Route::delete('{category}/destroy', 'CategoryController@destroy');
});

Route::prefix('/products')->group(function () {
    Route::get('', 'ProductController@index');
    Route::get('{product}', 'ProductController@show');
    Route::post('store', 'ProductController@store');
    Route::patch('{product}', 'ProductController@update');
    Route::post('destroy', 'CategoryController@destroyMass');
    Route::delete('{product}/destroy', 'ProductController@destroy');
});

Route::prefix('/choices')->group(function () {
    Route::get('{choices}', 'ChoicesController@show');
    Route::post('store', 'ChoicesController@store');
    Route::patch('{choices}', 'ChoicesController@update');
    Route::post('destroy', 'ChoicesController@destroyMass');
    Route::delete('{choices}/destroy', 'ChoicesController@destroy');
});

Route::prefix('/choicesGroup')->group(function () {
    Route::get('', 'ChoicesGroupController@index');
    Route::get('{choicesGroup}', 'ChoicesGroupController@show');
    Route::post('store', 'ChoicesGroupController@store');
    Route::patch('{choicesGroup}', 'ChoicesGroupController@update');
    Route::post('destroy', 'ChoicesGroupController@destroyMass');
    Route::delete('{choicesGroup}/destroy', 'ChoicesGroupController@destroy');
});

Route::prefix('/productGroups')->group(function () {
    Route::post('store', 'ProductGroupsController@store');
    Route::post('destroy', 'ProductGroupsController@removeProductGroups');
});

/*
 * Current user
 * */
Route::prefix('/user')->group(function () {
    Route::get('', 'CurrentUserController@show');
    Route::patch('', 'CurrentUserController@update');
    Route::patch('/password', 'CurrentUserController@updatePassword');
    Route::patch('/restaurant', 'RestaurantController@update');
});

/*
 * File upload (e.g. avatar)
 * */
Route::post('/files/store', 'FilesController@store');

//Slider

Route::prefix('/slider')->group(function () {
    Route::get('', 'SliderController@index');
    Route::get('{slider}', 'SliderController@show');
    Route::post('store', 'SliderController@store');
    Route::post('{slider}', 'SliderController@update');
    Route::delete('destroy', 'SliderController@destroyMass');
    Route::delete('{slider}/destroy', 'SliderController@destroy');
});

Route::prefix('/generalSetting')->group(function () {
    Route::get('', 'GeneralSettingsController@index');
    Route::get('{generalSettings}', 'GeneralSettingsController@show');
    Route::post('store', 'GeneralSettingsController@store');
    Route::post('{generalSettings}', 'GeneralSettingsController@update');
    Route::post('destroy', 'GeneralSettingsController@destroyMass');
    Route::delete('{generalSetting}/destroy', 'GeneralSettingsController@destroy');
});

//SMSSetting

Route::prefix('/SMSSetting')->group(function () {
    Route::get('', 'SmsSettingsController@index');
    Route::get('{SMSSettings}', 'SmsSettingsController@show');
    Route::post('store', 'SmsSettingsController@store');
    Route::patch('{SMSSettings}', 'SmsSettingsController@update');
    Route::post('destroy', 'SmsSettingsController@destroyMass');
    Route::delete('{SMSSettings}/destroy', 'SmsSettingsController@destroy');
});

//DeliveryCharges
Route::prefix('/DeliveryCharges')->group(function () {
    Route::get('', 'DeliveryChargesController@index');
    Route::get('{deliveryCharges}', 'DeliveryChargesController@show');
    Route::post('store', 'DeliveryChargesController@store');
    Route::patch('{deliveryCharges}', 'DeliveryChargesController@update');
    Route::post('destroy', 'DeliveryChargesController@destroyMass');
    Route::delete('{deliveryCharges}/destroy', 'DeliveryChargesController@destroy');
});

//$ourStory
Route::prefix('/OurStory')->group(function () {
    Route::get('', 'OurStoryController@index');
    Route::get('{ourStory}', 'OurStoryController@show');
    Route::post('store', 'OurStoryController@store');
    Route::post('{ourStory}', 'OurStoryController@update');
    Route::post('destroy', 'OurStoryController@destroyMass');
    Route::delete('{ourStory}/destroy', 'OurStoryController@destroy');
});

//Gallery
Route::prefix('/Gallery')->group(function () {
    Route::get('', 'GalleryController@index');
    Route::get('{gallery}', 'GalleryController@show');
    Route::post('store', 'GalleryController@store');
    Route::patch('{gallery}', 'GalleryController@update');
    Route::post('destroy', 'GalleryController@destroyMass');
    Route::delete('{gallery}/destroy', 'GalleryController@destroy');
});

//cms
Route::prefix('/cms')->group(function () {
    Route::get('', 'CMSController@index');
    Route::get('{cms}', 'CMSController@show');
    Route::post('store', 'CMSController@store');
    Route::patch('{cms}', 'CMSController@update');
    Route::post('destroy', 'CMSController@destroyMass');
    Route::delete('{cms}/destroy', 'CMSController@destroy');
});

Route::get('MessageTemplating', 'MessageTemplatingController@index');
//messageTemplating
Route::prefix('/MessageTemplating')->group(function () {

    Route::get('{messageTemplating}', 'MessageTemplatingController@show');
    Route::post('store', 'MessageTemplatingController@store');
    Route::patch('{messageTemplating}', 'MessageTemplatingController@update');
    Route::post('destroy', 'MessageTemplatingController@destroyMass');
    Route::delete('{messageTemplating}/destroy', 'MessageTemplatingController@destroy');
});

//cms
Route::prefix('/orders')->group(function () {
    Route::get('', 'OrderController@getAllOrders');
    Route::get('{id}', 'OrderController@show');
    Route::put('{id}', 'OrderController@updateOrderStatus');
});
Route::get('deals-products', 'DealsController@getDealProducts');
