<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'name',
        'description',
        'user_id',
        'status',
        'image'
    ];

    protected $with = ['products'];

    public function products() {
        return $this->hasMany('App\Product','id_category','id') ;
    }
}
