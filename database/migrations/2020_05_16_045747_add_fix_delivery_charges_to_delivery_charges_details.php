<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFixDeliveryChargesToDeliveryChargesDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_charges_details', function (Blueprint $table) {
            $table->float('fix_delivery_charges')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_charges_details', function (Blueprint $table) {
            $table->dropColumn('fix_delivery_charges');
        });
    }
}
