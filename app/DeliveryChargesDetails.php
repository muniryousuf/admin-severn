<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryChargesDetails extends Model
{
    protected $table = "delivery_charges_details";
    protected $fillable = ['postal_code','miles','amount','fix_delivery_charges'];

    public function deliveryCharges()
    {

        return $this->belongsTo(DeliveryCharges::class);
    }
}
