<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\ProductStoreRequest;
use App\Product;
use App\ProductSizes;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() {

        $products = Product::all();
        return response()->json(['data' => $products]);
    }

    /**
     * Store new resource
     *
     * @param ProductRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductStoreRequest $request ) {

        $requestData = $request->all();

        $product = new Product();
        $product->fill($request->all());
        $product->save();

        if($product->id) {
            if (count($requestData['inputs']) > 0) {
               foreach ($requestData['inputs'] as $input) {
                    ProductSizes::create(['id_product' => $product->id, 'size' => $input['size'], 'price' => $input['price']]);
               }
            }
        }

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $product->id
            ]
        ]);
    }

    /**
     * Delete resource
     *
     * @param Product Destroy $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        $product = Product::find($id);

        if($product) {
            $product->productGroups()->delete();
            $product->productSizes()->delete();

            $product->delete();
        }

        return response()->json([
            'status' => true,
            'deleted' => true,
            'data' => []
        ]);
    }
}
