import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/tables',
      name: 'tables',
      component: () => import('./views/Tables.vue')
    },
    {
      path: '/forms',
      name: 'forms',
      component: () => import('./views/Forms.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('./views/Profile.vue')
    },
    {
      path: '/clients/index',
      name: 'clients.index',
      component: () => import('./views/Clients/ClientsIndex.vue'),
    },
    {
      path: '/clients/new',
      name: 'clients.new',
      component: () => import('./views/Clients/ClientsForm.vue'),
    },
    {
      path: '/clients/:id',
      name: 'clients.edit',
      component: () => import('./views/Clients/ClientsForm.vue'),
      props: true
    },
    {
      path: '/offers/index',
      name: 'offers.index',
      component: () => import('./views/Offers/OffersIndex.vue'),
    },
    {
      path: '/orders/index',
      name: 'orders.index',
      component: () => import('./views/Orders/OrdersIndex.vue'),
    },
    {
      path: '/orders/:id',
      name: 'orders.edit',
      component: () => import('./views/Orders/OrdersForm.vue'),
      props: true
    },
    {
      path: '/offers/new',
      name: 'offers.new',
      component: () => import('./views/Offers/OffersForm.vue'),
    },
    {
      path: '/offers/:id',
      name: 'offers.edit',
      component: () => import('./views/Offers/OffersForm.vue'),
      props: true
    },
    {
      path: '/vouchers/index',
      name: 'vouchers.index',
      component: () => import('./views/Vouchers/VouchersIndex.vue'),
    },
    {
      path: '/vouchers/new',
      name: 'vouchers.new',
      component: () => import('./views/Vouchers/VouchersForm.vue'),
    },
    {
      path: '/vouchers/:id',
      name: 'vouchers.edit',
      component: () => import('./views/Vouchers/VouchersForm.vue'),
      props: true
    },
    {
      path: '/deals/index',
      name: 'deals.index',
      component: () => import('./views/Deals/DealsIndex.vue'),
    },
    {
      path: '/deals/new',
      name: 'deals.new',
      component: () => import('./views/Deals/DealsForm.vue'),
    },
    {
      path: '/deals/:id',
      name: 'deals.edit',
      component: () => import('./views/Deals/DealsForm.vue'),
      props: true
    },
    {
      path: '/menusetup/index',
      name: 'menusetup.index',
      component: () => import('./views/MenuSetup/MenuSetup.vue'),

    },
    {
      path: '/address/index',
      name: 'address.index',
      component: () => import('./views/Restaurant/AddressForm.vue'),
    },
    {
      path: '/website/index',
      name: 'website.index',
      component: () => import('./views/Restaurant/WebsiteForm.vue'),
    },

    //slider
    {
      path: '/slider/index',
      name: 'slider.index',
      component: () => import('./views/Slider/SliderIndex.vue'),
    },
    {
      path: '/slider/new',
      name: 'slider.new',
      component: () => import('./views/Slider/SliderForm.vue'),
    },
    {
      path: '/slider/:id',
      name: 'slider.edit',
      component: () => import('./views/Slider/SliderForm.vue'),
      props: true
    },
    // GeneralSetting
    {
      path: '/GeneralSetting/index',
      name: 'general_setting.index',
      component: () => import('./views/GeneralSetting/GeneralSettingIndex.vue'),
    },
    {
      path: '/GeneralSetting/new',
      name: 'general_setting.new',
      component: () => import('./views/GeneralSetting/GeneralSettingForm.vue'),
    },
    {
      path: '/GeneralSetting/:id',
      name: 'general_setting.edit',
      component: () => import('./views/GeneralSetting/GeneralSettingForm.vue'),
      props: true
    },
    //SMSSetting
    {
      path: '/SMSSetting/index',
      name: 'SMSSetting.index',
      component: () => import('./views/SMSSetting/SMSSettingIndex.vue'),
    },
    {
      path: '/SMSSetting/new',
      name: 'SMSSetting.new',
      component: () => import('./views/SMSSetting/SMSSettingForm.vue'),
    },
    {
      path: '/SMSSetting/:id',
      name: 'SMSSetting.edit',
      component: () => import('./views/SMSSetting/SMSSettingForm.vue'),
      props: true
    },
//Delivery Charges

    //SMSSetting
    {
      path: '/DeliveryCharges/index',
      name: 'DeliveryCharges.index',
      component: () => import('./views/DeliveryCharges/DeliveryChargesIndex.vue'),
    },
    {
      path: '/DeliveryCharges/new',
      name: 'DeliveryCharges.new',
      component: () => import('./views/DeliveryCharges/DeliveryChargesForm.vue'),
    },
    {
      path: '/DeliveryCharges/:id',
      name: 'DeliveryCharges.edit',
      component: () => import('./views/DeliveryCharges/DeliveryChargesForm.vue'),
      props: true
    },

    //OurStory
    {
      path: '/OurStory/index',
      name: 'OurStory.index',
      component: () => import('./views/OurStory/OurStoryIndex.vue'),
    },
    {
      path: '/OurStory/new',
      name: 'OurStory.new',
      component: () => import('./views/OurStory/OurStoryForm.vue'),
    },
    {
      path: '/OurStory/:id',
      name: 'OurStory.edit',
      component: () => import('./views/OurStory/OurStoryForm.vue'),
      props: true
    },

    //Gallery
    {
      path: '/Gallery/index',
      name: 'Gallery.index',
      component: () => import('./views/Gallery/GalleryIndex.vue'),
    },
    {
      path: '/Gallery/new',
      name: 'Gallery.new',
      component: () => import('./views/Gallery/GalleryForm.vue'),
    },
    {
      path: '/Gallery/:id',
      name: 'Gallery.edit',
      component: () => import('./views/Gallery/GalleryForm.vue'),
      props: true
    },

    //cms
    {
      path: '/cms/index',
      name: 'cms.index',
      component: () => import('./views/CMS/CMSIndex.vue'),
    },
    {
      path: '/cms/new',
      name: 'cms.new',
      component: () => import('./views/CMS/CMSForm.vue'),
    },
    {
      path: '/cms/:id',
      name: 'cms.edit',
      component: () => import('./views/CMS/CMSForm.vue'),
      props: true
    },

    //Messsage
    {
      path: '/MessageTemplating/index',
      name: 'MessageTemplating.index',
      component: () => import('./views/MessageTemplating/MessageTemplatingIndex.vue'),
    },
    {
      path: '/MessageTemplating/new',
      name: 'MessageTemplating.new',
      component: () => import('./views/MessageTemplating/MessageTemplatingForm.vue'),
    },
    {
      path: '/MessageTemplating/:id',
      name: 'MessageTemplating.edit',
      component: () => import('./views/MessageTemplating/MessageTemplatingForm.vue'),
      props: true
    },
    //SMSBroadcasting
    {
      path: '/SMSBroadcasting/new',
      name: 'SMSBroadcasting.new',
      component: () => import('./views/SMSBroadcasting/SMSBroadcastingForm.vue'),
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
