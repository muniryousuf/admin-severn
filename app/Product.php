<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'name',
        'price',
        'description',
        'food_allergy',
        'id_category',
        'status'
    ];

    protected $with = ['productGroups', 'productSizes'];

    public function productGroups() {
        return $this->hasMany('App\ProductGroups','id_product','id') ;
    }

    public function productSizes() {
        return $this->hasMany('App\ProductSizes','id_product','id') ;
    }
}
