<?php

namespace App\Http\Controllers;

use App\Http\Requests\SmsSettingseRequest;
use App\SmsSetting;
use Illuminate\Http\Request;

class SmsSettingsController extends Controller
{   /**
 * Create a new controller instance.
 *
 * @return void
 */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() {

        $smsSetting = SmsSetting::all();
        return response()->json(['data' => $smsSetting]);
    }

    public function show(SmsSetting $SMSSettings) {

        return response()->json([
            'data' => $SMSSettings
        ]);
    }

    public function store(SmsSettingseRequest $request )
    {
        $smsSetting = new SmsSetting();
        $smsSetting->fill($request->all());
        $smsSetting->save();

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $smsSetting->id
            ]
        ]);
    }
    public function update(Request $request, SmsSetting $SMSSettings ) {
        $requestData = $request->all();

        $SMSSettings->fill($request->all());
        $SMSSettings->save();
        return response()->json([
            'status' => true,
            'data' => $SMSSettings
        ]);
    }


    public function destroy($id) {

        $smsSetting = SmsSetting::find($id);

        if($smsSetting) {
            $smsSetting->delete();
        }

        return response()->json([
            'status' => true,
            'deleted' => true,
            'data' => []
        ]);
    }


    public function destroyMass( Request $request ) {

      $request->validate([
            'customer_id' => 'required|array'
        ]);

        SmsSetting::destroy($request->customer_id);

        return response()->json([
            'status' => true
        ]);
    }
}
