<?php

namespace App\Http\Controllers;

use App\Category;
use App\Deal;
use App\DealProducts;
use App\Http\Requests\DealStoreRequest;
use App\Product;
use Illuminate\Http\Request;

class DealsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Index resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $deals = Deal::get();

        return response()->json([
            'data' => $deals
        ]);
    }

    /**
     * Get single resource
     *
     * @param Vouchers $Deals
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show( Deal $deal ) {

        return response()->json([
            'data' => $deal
        ]);
    }

    /**
     * Update single resource
     *
     * @param VoucherStoreRequest $request
     * @param Voucher $deal
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( DealStoreRequest $request, Deal $deal ) {

        $requestData = $request->all();
        $deal->fill($request->all());
        $deal->save();
        if($deal->id) {
            $deal->products()->delete();

            if (count($requestData['products']) > 0) {
                foreach ($requestData['products'] as $product) {
                    DealProducts::create(['id_deal' => $deal->id, 'item_id' =>$product['item_id'], 'quantity' => $product['quantity'],'is_category'=> $product['is_category'],'is_options'=>$product['is_options']]);
                }
            }
        }

        return response()->json([
            'status' => true,
            'data' => $deal
        ]);
    }

    /**
     * Store new resource
     *
     * @param VoucherStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store( DealStoreRequest $request ) {

        $requestData = $request->all();



        $deal = new Deal;
        $deal->fill($request->all());
        $deal->save();


        if($deal->id) {
            if (count($requestData['products']) > 0) {
                foreach ($requestData['products'] as $product) {

                    DealProducts::create(['id_deal' => $deal->id, 'item_id' =>$product['item_id'], 'quantity' => $product['quantity'],'is_category'=> $product['is_category'] ,'is_options'=>$product['is_options']]);

                }
            }
        }

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $deal->id
            ]
        ]);
    }

    /**
     * Destroy single resource
     *
     * @param Voucher $deal
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy( Deal $deal ) {
        $deal->delete();

        return response()->json([
            'status' => true
        ]);
    }

    /**
     * Destroy resources by ids
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroyMass( Request $request ) {
        $request->validate([
            'ids' => 'required|array'
        ]);

        Deal::destroy($request->ids);

        return response()->json([
            'status' => true
        ]);
    }
    public function getDealProducts(){

        $categories =Category::all()->toArray();
        $products = Product::all()->toArray();

        $allProducts  = array();

        foreach ($categories as $c){
            $allProducts[] = array('id'=>'cat_'.$c['id'],'name'=>$c['name']."(Category)");
        }

        foreach ($products as $c){
            $allProducts[] = array('id'=>"pro_".$c['id'],'name'=>$c['name']."(Product)");
        }

        return response()->json(['data' => $allProducts]);

    }
}
