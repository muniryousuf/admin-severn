<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OurStory extends Model
{
    protected $table ="our_story";
    protected $fillable = ['main_title','image','all_title','description'];
}
