<?php

namespace App\Http\Controllers;

use App\Http\Requests\PasswordUpdateRequest;
use App\Http\Requests\RestaurantUpdateRequest;
use App\Restaurant;
use App\RestaurantAddress;
use App\User;
use Faker\Provider\Address;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RestaurantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Update User Restaurant
     *
     * @param RestaurantUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(RestaurantUpdateRequest $request ) {

        $user = $request->user();

        if($user->restaurant != null) {


            /*if($requestData['logo'] != null) {
                $imageName = time().'.'.$request->image->getClientOriginalExtension();
                $requestData['image'] = $imageName;
                $request->image->move(public_path('images/category'), $imageName);
            }*/


            $user->restaurant->fill($request->restaurant);
            $user->restaurant->address->fill($request->restaurant['address']);
            $user->restaurant->save();
            $user->restaurant->address->save();
        } else {

            $restaurant = new Restaurant();
            $restaurant->fill($request->restaurant);
            $restaurant->save();

            if($restaurant) {
                $address = new RestaurantAddress();
                $address->fill(['id_restaurant' => $restaurant->id, 'zip_code' => $request->restaurant['address']['zip_code'], 'city' => $request->restaurant['address']['city'], 'address' => $request->restaurant['address']['address'], 'id_country' => $request->restaurant['address']['id_country']]);
                $address->save();
            }
            $user = User::find($user->id);
        }

        return response()->json([
            'data' => $user
        ]);
    }

    /**
     * Update current user's password
     *
     * @param PasswordUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword( PasswordUpdateRequest $request ) {
        $user = $request->user();
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json([
            'status' => true
        ]);
    }
}
