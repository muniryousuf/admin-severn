<?php

namespace App\Http\Controllers;

use App\GeneralSettings;
use App\Http\Requests\GeneralSettingsStoreRequest;
use Illuminate\Http\Request;

class GeneralSettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() {

        $generalSettings = GeneralSettings::get();
        //unset($generalSettings[0]->header_logo, $generalSettings[0]->footer_logo, $generalSettings[0]->favicon);
        // $generalSettings=unset();
        return response()->json(['data' => $generalSettings]);
    }

    public function show(GeneralSettings $generalSettings) {

        return response()->json([
            'data' => $generalSettings
        ]);
    }

    public function store(GeneralSettingsStoreRequest $request )
    {
        $requestData = $request->all();

        if ($requestData['header_logo'] != null && !isset($requestData['header_logo'])) {
            $imageName = time() . '.' . $request->header_logo->getClientOriginalExtension();
            $requestData['header_logo'] = $imageName;
            $request->header_logo->move(public_path('images/header_logo'), $imageName);
        }
        if ($requestData['footer_logo'] != null && !isset($requestData['footer_logo'])) {
            $imageName = time() . '.' . $request->footer_logo->getClientOriginalExtension();
            $requestData['footer_logo'] = $imageName;
            $request->footer_logo->move(public_path('images/footer_logo'), $imageName);
        }
        if ($requestData['favicon'] != null && !isset($requestData['favicon'])) {
            $imageName = time() . '.' . $request->favicon->getClientOriginalExtension();
            $requestData['favicon'] = $imageName;
            $request->favicon->move(public_path('images/favicon'), $imageName);
        }


        $generalSettings = new GeneralSettings();
        $generalSettings->fill($requestData);
        $generalSettings->save();

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $generalSettings->id
            ]
        ]);
    }

    public function update(Request $request, GeneralSettings $generalSettings ) {

        $requestData = $request->all();

        /*if ($requestData['favicon'] != null || $requestData['favicon'] != "null") {
            $imageName = time() . '.' . $request->favicon->getClientOriginalExtension();
            $requestData['favicon'] = $imageName;
            $request->favicon->move(public_path('images/favicon'), $imageName);
        }
        if ($requestData['footer_logo'] != null || $requestData['header_logo'] != 'undefined') {
            $imageName = time() . '.' . $request->footer_logo->getClientOriginalExtension();
            $requestData['footer_logo'] = $imageName;
            $request->footer_logo->move(public_path('images/footerLogo'), $imageName);
        }
        if ($requestData['header_logo'] != null || $requestData['header_logo'] != 'undefined') {
            $imageName = time() . '.' . $request->header_logo->getClientOriginalExtension();
            $requestData['header_logo'] = $imageName;
            $request->header_logo->move(public_path('images/headerLogo'), $imageName);
        }*/

        $generalSettings->fill($request->all());
        $generalSettings->save();
        return response()->json([
            'status' => true,
            'data' => $generalSettings
        ]);
    }

    public function destroy($id) {

        $generalSettings = GeneralSettings::find($id);

        if($generalSettings) {
            $generalSettings->delete();
        }

        return response()->json([
            'status' => true,
            'deleted' => true,
            'data' => []
        ]);
    }


    public function destroyMass( Request $request ) {
        $request->validate([
            'ids' => 'required|array'
        ]);

        GeneralSettings::destroy($request->ids);

        return response()->json([
            'status' => true
        ]);
    }
}
