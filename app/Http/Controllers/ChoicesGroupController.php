<?php

namespace App\Http\Controllers;

use App\ChoicesGroup;
use App\Http\Requests\ChoicesGroupStoreRequest;

class ChoicesGroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Index resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {

        $choices = ChoicesGroup::all();
        return response()->json(['data' => $choices]);
    }

    /**
     * Store new resource
     *
     * @param ChoicesGroupStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ChoicesGroupStoreRequest $request ) {
        $choices = new ChoicesGroup();
        $choices->fill($request->all());
        $choices->save();

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $choices->id
            ]
        ]);
    }
}
