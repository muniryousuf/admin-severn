<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageTemplating extends Model
{
    //
    protected $table = 'message_templating';
    protected $fillable = ['template_slug','description', 'status'];
}
