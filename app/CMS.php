<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMS extends Model
{
    protected $table = 'cms';
    protected $fillable = ['title','content', 'slug','meta_title','meta_desc','index_follow'];
}
