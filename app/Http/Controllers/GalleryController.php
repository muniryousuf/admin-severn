<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() {

        $gallery = Gallery::all();
        return response()->json(['data' => $gallery]);
    }

    public function show(Gallery $gallery) {

        return response()->json([
            'data' => $gallery
        ]);
    }

    public function store(Request $request )
    {

        $requestData = $request->all();
dd($requestData);
        if($requestData['image'] != null) {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $requestData['image'] = $imageName;
            $request->image->move(public_path('images/slider'), $imageName);
        }
        $gallery = new Gallery();
        $gallery->fill($request->all());
        $gallery->save();

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $gallery->id
            ]
        ]);
    }
    public function update(Request $request, Gallery $gallery ) {
        $requestData = $request->all();

        $gallery->fill($request->all());
        $gallery->save();
        return response()->json([
            'status' => true,
            'data' => $gallery
        ]);
    }


    public function destroy($id) {

        $gallery = Gallery::find($id);

        if($gallery) {
            $gallery->delete();
        }

        return response()->json([
            'status' => true,
            'deleted' => true,
            'data' => []
        ]);
    }


    public function destroyMass( Request $request ) {

        $request->validate([
            'id' => 'required|array'
        ]);

        Gallery::destroy($request->id);

        return response()->json([
            'status' => true
        ]);
    }
}
